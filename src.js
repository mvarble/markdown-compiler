/**
 * src.js
 *
 * This module will be converted by babel in order to create the package.
 */

/**
 * This is a processor powered by Unified which allows us to convert
 * Markdown+HTML+KaTeX into a Hast tree which encodes the compilation.
 */
import unified from 'unified';
import remark from 'remark-parse';
import math from 'remark-math';
import remark2rehype from 'remark-rehype';
import raw from 'rehype-raw';
import katex from 'rehype-katex';
import toSnabbdom from 'hast-util-to-snabbdom';

const processor = unified()
  .use(remark, { allowDangerousHTML: true })
  .use(math)
  .use(remark2rehype, { allowDangerousHTML: true })
  .use(raw)
  .use(katex)
  .use(() => toSnabbdom);

export { processor };

/**
 * makeMarkdownCompiler
 *
 * This is our cycle.js compiler app. It uses the @cycle/state functionality to
 * create a reducer which reads the state of components that are mounted on the 
 * lifecycle of a markdown stream.
 */

// module dependencies: npm packages
import xs from 'xstream';
import isolate from '@cycle/isolate';
import visit from 'unist-util-visit';

function makeMarkdownCompiler(components) {
  // it returns an app; to use it, be sure it has the withState from 
  // @cycle/state (or some parent does in a composition).
  return app

  // this is a "unist-is test" for if a 
  function customTag (node) {
    return Object.keys(components).some(k => (
      (k == node.sel) || (node.data && node.data.componentId)
    ));
  }

  // the app
  function app(sources) {
    // map markdown stream to that of a (stale) snabbdom tree
    const tree$ = sources.markdown.map(md => xs.fromPromise(
      processor.run(processor.parse(md))
    )).flatten();

    // map each stale tree to a new stream of dispatchers and other sinks
    const sink$ = tree$.map(tree => {
      // create a copy of the tree
      const linkedTree = { ...tree };

      // extract the custom tags and link accordingly
      let staleNodes = [];
      let runningId = 0;
      visit(linkedTree, customTag, (node, index, parent) => {
        parent.children[index].data.componentId = `${node.sel}-${runningId}`;
        staleNodes.push(parent.children[index]);
        runningId += 1;
      })
      
      // create the inital reducer
      const initState = staleNodes.reduce((obj, node) => ({ 
        ...obj, 
        [node.data.componentId]: {},
      }), { DOM: tree, complete: false });
      const initReducer$ = xs.of(() => initState);

      // create components for each custom tag and collect sinks
      const sinksList = staleNodes.map(node => {
        const newSources = { ...sources, staleDom: xs.of(node) };
        return isolate(components[node.sel], node.data.componentId)(newSources);
      });

      // create the reducer
      const rdcr$List = sinksList.filter(s => s.state).map(s => s.state);
      const reducer$ = xs.combine(...rdcr$List).map(reducers => (oldState => {
        // first update the state as according to the reducers
        const bigReducer = reducers.reduce((f, g) => (a => g(f(a))), a => a);
        const state = bigReducer(oldState);

        // get a copy of the DOM
        const DOM = { ...state.DOM };

        // manipulate the tree to the (potentially) updated components
        visit(DOM, customTag, (node, index, parent) => {
          // see if there is a DOM for the current component
          if (state[node.data.componentId].DOM) {
            const newNode = { ...state[node.data.componentId].DOM };
            newNode.data.componentId = node.data.componentId;
            parent.children.splice(index, 1, newNode);
          }
        });

        // calculate the completion of all components
        const completeList = Object.keys(state).filter(k => (
          k !== 'DOM' 
          && k !== 'complete' 
          && typeof state[k].complete !== 'undefined'
        )).map(k => state[k].complete);
        const complete = completeList.length ? completeList.every(a => a) : true;

        // update the state accordingly
        return { ...state, DOM, complete };
      }));

      // create the HTTP stream from the components
      const req$List = sinksList.filter(s => s.HTTP).map(s => s.HTTP);
      const request$ = req$List.length ? xs.merge(...req$List) : xs.empty();

      return { state: xs.merge(initReducer$, reducer$), HTTP: request$ };
    });

    // flatten and parse the stream of sinks
    return {
      state: sink$.map(s => s.state).flatten(),
      HTTP: sink$.map(s => s.HTTP).flatten(),
      log: sources.state.stream.map(state => state.DOM),
    }
  }
}
export { makeMarkdownCompiler };
export default makeMarkdownCompiler;
