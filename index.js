"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeMarkdownCompiler = makeMarkdownCompiler;
exports["default"] = exports.processor = void 0;

var _unified = _interopRequireDefault(require("unified"));

var _remarkParse = _interopRequireDefault(require("remark-parse"));

var _remarkMath = _interopRequireDefault(require("remark-math"));

var _remarkRehype = _interopRequireDefault(require("remark-rehype"));

var _rehypeRaw = _interopRequireDefault(require("rehype-raw"));

var _rehypeKatex = _interopRequireDefault(require("rehype-katex"));

var _hastUtilToSnabbdom = _interopRequireDefault(require("hast-util-to-snabbdom"));

var _xstream = _interopRequireDefault(require("xstream"));

var _isolate = _interopRequireDefault(require("@cycle/isolate"));

var _unistUtilVisit = _interopRequireDefault(require("unist-util-visit"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var processor = (0, _unified["default"])().use(_remarkParse["default"], {
  allowDangerousHTML: true
}).use(_remarkMath["default"]).use(_remarkRehype["default"], {
  allowDangerousHTML: true
}).use(_rehypeRaw["default"]).use(_rehypeKatex["default"]).use(function () {
  return _hastUtilToSnabbdom["default"];
});
exports.processor = processor;

function makeMarkdownCompiler(components) {
  // it returns an app; to use it, be sure it has the withState from 
  // @cycle/state (or some parent does in a composition).
  return app; // this is a "unist-is test" for if a 

  function customTag(node) {
    return Object.keys(components).some(function (k) {
      return k == node.sel || node.data && node.data.componentId;
    });
  } // the app


  function app(sources) {
    // map markdown stream to that of a (stale) snabbdom tree
    var tree$ = sources.markdown.map(function (md) {
      return _xstream["default"].fromPromise(processor.run(processor.parse(md)));
    }).flatten(); // map each stale tree to a new stream of dispatchers and other sinks

    var sink$ = tree$.map(function (tree) {
      // create a copy of the tree
      var linkedTree = _objectSpread({}, tree); // extract the custom tags and link accordingly


      var staleNodes = [];
      var runningId = 0;
      (0, _unistUtilVisit["default"])(linkedTree, customTag, function (node, index, parent) {
        parent.children[index].data.componentId = "".concat(node.sel, "-").concat(runningId);
        staleNodes.push(parent.children[index]);
        runningId += 1;
      }); // create the inital reducer

      var initState = staleNodes.reduce(function (obj, node) {
        return _objectSpread({}, obj, _defineProperty({}, node.data.componentId, {}));
      }, {
        DOM: tree,
        complete: false
      });

      var initReducer$ = _xstream["default"].of(function () {
        return initState;
      }); // create components for each custom tag and collect sinks


      var sinksList = staleNodes.map(function (node) {
        var newSources = _objectSpread({}, sources, {
          staleDom: _xstream["default"].of(node)
        });

        return (0, _isolate["default"])(components[node.sel], node.data.componentId)(newSources);
      }); // create the reducer

      var rdcr$List = sinksList.filter(function (s) {
        return s.state;
      }).map(function (s) {
        return s.state;
      });

      var reducer$ = _xstream["default"].combine.apply(_xstream["default"], _toConsumableArray(rdcr$List)).map(function (reducers) {
        return function (oldState) {
          // first update the state as according to the reducers
          var bigReducer = reducers.reduce(function (f, g) {
            return function (a) {
              return g(f(a));
            };
          }, function (a) {
            return a;
          });
          var state = bigReducer(oldState); // get a copy of the DOM

          var DOM = _objectSpread({}, state.DOM); // manipulate the tree to the (potentially) updated components


          (0, _unistUtilVisit["default"])(DOM, customTag, function (node, index, parent) {
            // see if there is a DOM for the current component
            if (state[node.data.componentId].DOM) {
              var newNode = _objectSpread({}, state[node.data.componentId].DOM);

              newNode.data.componentId = node.data.componentId;
              parent.children.splice(index, 1, newNode);
            }
          }); // calculate the completion of all components

          var completeList = Object.keys(state).filter(function (k) {
            return k !== 'DOM' && k !== 'complete' && typeof state[k].complete !== 'undefined';
          }).map(function (k) {
            return state[k].complete;
          });
          var complete = completeList.length ? completeList.every(function (a) {
            return a;
          }) : true; // update the state accordingly

          return _objectSpread({}, state, {
            DOM: DOM,
            complete: complete
          });
        };
      }); // create the HTTP stream from the components


      var req$List = sinksList.filter(function (s) {
        return s.HTTP;
      }).map(function (s) {
        return s.HTTP;
      });
      var request$ = req$List.length ? _xstream["default"].merge.apply(_xstream["default"], _toConsumableArray(req$List)) : _xstream["default"].empty();
      return {
        state: _xstream["default"].merge(initReducer$, reducer$),
        HTTP: request$
      };
    }); // flatten and parse the stream of sinks

    return {
      state: sink$.map(function (s) {
        return s.state;
      }).flatten(),
      HTTP: sink$.map(function (s) {
        return s.HTTP;
      }).flatten(),
      log: sources.state.stream.map(function (state) {
        return state.DOM;
      })
    };
  }
}

var _default = makeMarkdownCompiler;
exports["default"] = _default;
